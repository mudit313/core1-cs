﻿using UnityEngine;
using System.Collections;

public class Behavior : MonoBehaviour {
	public string Menu;//,Buymenu,Original;
	//public Running_animation run;
	public GameObject knife,pistol,no_weapon;
	public static GameObject weapon1,weapon2,weapon3;
	public GameObject bomb;
	public Transform buymenu;
	bool b = false;
	// Use this for initialization
	void Start () {
		Cursor.visible = false;
		weapon1 = knife;
		weapon2 = knife;
		weapon3 = no_weapon;
	}
	
	// Update is called once per frame
	void Update () {



		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Application.LoadLevelAdditive(Menu);

		}
		if (Input.GetKeyDown(KeyCode.B))
		{
				//Application.LoadLevelAdditive(Buymenu);
			swapweapons();
			buymenu.gameObject.SetActive(true);
			Cursor.visible = true;
		}
	
		if (Input.GetKey (KeyCode.W)) {
			//run.running();
		}
		if (Input.GetKey(KeyCode.Mouse1)) {
			//run.Sniper_Zoom();
		}
		if (Input.GetKeyDown(KeyCode.Q))
		{
			swapweapons();
		}
		if (Input.GetKeyDown (KeyCode.E)) {
			if(!b){
				b = true;
				weapon1.SetActive (false);
				weapon2.SetActive (false);
				bomb.SetActive(true);
			}
			else{
				b = false;
				weapon1.SetActive (false);
				weapon2.SetActive (true);
				bomb.SetActive(false);
			}
		}

		if (Input.GetKeyDown (KeyCode.Alpha3)) {
			weapon1.SetActive (false);
			weapon2.SetActive (false);
			weapon3.SetActive(true);
		}
	}

	void swapweapons()
	{
		//if (weapon2.activeInHierarchy) {
			if (weapon1.activeSelf == true) {
				weapon1.SetActive (false);
				weapon2.SetActive (true);
			} else {
				weapon1.SetActive (true);
				weapon2.SetActive (false);
			}
		//}
	}
}
