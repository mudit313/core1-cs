﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BulletCount : MonoBehaviour {
	public int Bullet = 10;
	public Text Bullettext;
	public AudioSource fire;
	public AudioSource reload;

	// Update is called once per frame
	void Update () {
		if (Bullet > 0) {
			if (Input.GetMouseButtonDown (0)) {
				Bullet -= 1;
				fire.Play();
			}
		}
		Bullettext.text = Bullet.ToString ();
		if (Input.GetKey (KeyCode.R)) {
			reload.Play();
			if(!reload.isPlaying)
				Bullet = 10;
		}
	}
}
