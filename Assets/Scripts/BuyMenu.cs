﻿using UnityEngine;
using System.Collections;


public class BuyMenu : MonoBehaviour {

	void Start () {
		Cursor.visible = true;
	}


	public Transform buyMenu;
	public Texture aTexture;
	public GameObject rifle, pistol, sniper,Grenade;
	public GameObject Deagle, AWP, AK, Default;


	public void Changeweapon(int value){
		switch (value) {
		case 1:
			Behavior.weapon2 = pistol;
			//Debug.Log(Behavior.weapon2.name);
			//pistol.SetActive(true);
			break;
		case 2: 
			Behavior.weapon2 = rifle;
			//rifle.SetActive(true);
			break;
		case 3:
			Behavior.weapon2 = sniper;
			//sniper.SetActive(true);
			break;
		case 4:
			Behavior.weapon3 = Grenade;
			break;
		}

	}
	public void ResumeGame(bool clicked)
	{
		if (clicked == true) {
			buyMenu.gameObject.SetActive(false);
			Cursor.visible = false;
		} else {
			buyMenu.gameObject.SetActive (true);
		}
	}

	public void DisplayImage(int value)
	{
		switch (value) {
		case 1:
			Deagle.SetActive(true);
			AWP.SetActive (false);
			AK.SetActive(false);
			Default.SetActive(false);
			break;
		case 2: 
			Deagle.SetActive(false);
			AK.SetActive(true);
			AWP.SetActive (false);
			Default.SetActive(false);
			
			break;
		case 3:
			Deagle.SetActive(false);
			AK.SetActive(false);
			AWP.SetActive (true);
			Default.SetActive(false);
			
			break;
		 default:
			Deagle.SetActive(false);
			AK.SetActive(false);
			AWP.SetActive (false);
			Default.SetActive(true);

			break;

		}
	}

	public void RImage(int value)
	{
		switch (value) {
		case 1:
			Deagle.SetActive(false);
			AK.SetActive(false);
			AWP.SetActive (false);
			Default.SetActive(true);
		
			break;
		case 2: 
			Deagle.SetActive(false);
			AK.SetActive(false);
			AWP.SetActive (false);
			Default.SetActive(true);
			
			break;
		case 3:
			Deagle.SetActive(false);
			AK.SetActive(false);
			AWP.SetActive (false);
			Default.SetActive(true);
			
			break;
		}
	}
}
