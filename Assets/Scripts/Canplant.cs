﻿using UnityEngine;
using System.Collections;

public class Canplant : MonoBehaviour {
	public static bool plant = false;

	void OnTriggerEnter(Collider other) {
		plant = true;
		Debug.Log("Can Plant");
	}
	
	void OnTriggerExit(Collider other) {
		plant = false;
		Debug.Log("Cannot plant");
	}
}
