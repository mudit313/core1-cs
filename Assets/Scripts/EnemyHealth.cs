﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EnemyHealth : MonoBehaviour {
	public int Health = 100;
	public Text healthtext;

	// Update is called once per frame
	void Update () {
		if (Health <= 0) {
			Dead();
		}
		healthtext.text = Health.ToString ();
	}

	public void ApplyDamage(int damage)
	{
		Health -= damage;
	}

	void Dead(){
		Destroy (gameObject);
	}
}