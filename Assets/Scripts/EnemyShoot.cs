﻿using UnityEngine;
using System.Collections;

public class EnemyShoot : MonoBehaviour {
	public GameObject player;
	public float t = 0f;
	//public Transform kuchbhi;

	void Update(){
		t += Time.deltaTime;
		//kuchbhi.position = new Vector3(kuchbhi.position.x * Random.Range(-10,10), kuchbhi.position.y * Random.Range(-10,10), kuchbhi.position.z * Random.Range(-10,10));
		if (t > 1f) {
			Shoot (player, 20);
			transform.LookAt (player.transform);
		}
			//Debug.Log(t);
			//t -= 2.0f;
	}

	void Shoot(GameObject target, int damage)
	{
		RaycastHit hit;
		if(Physics.Raycast (transform.position, transform.TransformDirection(Vector3.forward), out hit))
		{
			Debug.DrawLine (transform.position, hit.point, Color.red);
			Debug.Log("Shoot");
			hit.transform.SendMessage("ApplyDamage", damage, SendMessageOptions.DontRequireReceiver);
			t = 0f;
		}
	}
}
