﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class Explosive : MonoBehaviour {
	public float downtime;
	public bool planted = false;
	public int div = 0;
	public int timer;
	public float t = 0f;
	public Text timetext;
	public AudioSource exp;
	public GameObject x;
	//public bool canplant = false;

	void Update () {
		if (Input.GetMouseButtonDown (0))
			downtime = Time.time;
		if (Input.GetMouseButton(0) && (Time.time - downtime) > 2f && Canplant.plant)
		{
			transform.parent = null;
			transform.position = new Vector3 (transform.position.x, 0f, transform.position.z);
			transform.rotation = Quaternion.Euler(90,0,0);
			planted = true;
			x.SetActive(true);
			timer = 5;
			//InvokeRepeating("redtime", 1, 1f);
		}
		if (planted == true) {
			Invoke ("Destroy", 5);
			Invoke ("Pla", 3);
			t += Time.deltaTime;
			if(t > 1.0f)
			{
				timer -= 1;
				t -= 1.0f;
			}
		}
		timetext.text = timer.ToString ();
	}

	void Destroy(){
		Destroy (gameObject);
		planted = false;
		//exp.Play ();
	}

	void Pla(){
		exp.Play ();
	}
}
