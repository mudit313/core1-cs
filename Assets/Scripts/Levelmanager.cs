﻿using UnityEngine;
using System.Collections;


public class Levelmanager : MonoBehaviour {


	void Start () {
		Cursor.visible = true;
	}


	public Transform mainMenu, optionsMenu;

	public void ResumeGame()
	{
		Application.UnloadLevel("Levelmanager");
		Cursor.visible = false;
	}

	public void LoadScene(string name){
		Application.LoadLevel (name);
}
	public void QuitGame(){
		Application.UnloadLevel ("Levelmanager");
		Application.Quit ();
	}
	public void OptionsMenu(bool clicked)
	{
		if (clicked == true) {
			optionsMenu.gameObject.SetActive(clicked);
			mainMenu.gameObject.SetActive(false);
		} else {
			optionsMenu.gameObject.SetActive(clicked);
			mainMenu.gameObject.SetActive(true);
		}
	}
}
