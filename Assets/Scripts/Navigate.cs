﻿using UnityEngine;
using System.Collections;

public class Navigate : MonoBehaviour {
	
	public Transform goal;
	NavMeshAgent agent;
	public float dist;
	public GameObject ray;

	void Update () {
		agent = GetComponent<NavMeshAgent>();
		dist = Vector3.Distance (agent.gameObject.transform.position, goal.position);
		if (dist > 5f) {
			agent.destination = goal.position;
			ray.SetActive (false);
		} else {
			agent.destination = agent.gameObject.transform.position;
			ray.SetActive (true);
		}
	}
}
