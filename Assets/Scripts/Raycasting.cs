﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Raycasting : MonoBehaviour {
	public int damage = 50;
	//public float dist;
	//public float maxdist = 1000f;
	public GameObject system;

	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Fire1"))
		{
			GetComponent<Animation>().Play ();
			RaycastHit hit;
			if(Physics.Raycast (system.transform.position, system.transform.TransformDirection(Vector3.forward), out hit))
			{
				Debug.DrawLine (transform.position, hit.point, Color.cyan);
				//dist = hit.distance;
				//Debug.Log(dist);
				//if (dist < maxdist)
				//{
					hit.transform.SendMessage("ApplyDamage", damage, SendMessageOptions.DontRequireReceiver);
				//}
			}
		}
	}
}