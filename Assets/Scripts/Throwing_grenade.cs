﻿using UnityEngine;
using System.Collections;

public class Throwing_grenade : MonoBehaviour {

	public Rigidbody grenade;
	public float velocity;
	private bool flag;
	// Use this for initialization
	void Start () {
		grenade = GetComponent<Rigidbody> ();
		flag = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Mouse0) & flag) {
			grenade.velocity = transform.forward * velocity;
			flag =false;
		}
	}
}
