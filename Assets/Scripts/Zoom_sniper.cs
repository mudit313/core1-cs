﻿using UnityEngine;
using System.Collections;

public class Zoom_sniper : MonoBehaviour {
	public bool zoomed = false;
	public Camera c;
	public int fov = 10;
	// Update is called once per frame
	void Update () {
		//Debug.Log (c.fieldOfView);
		if (Input.GetMouseButtonDown (1))
			zoomed = !zoomed;
		if(zoomed)
			c.fieldOfView = Mathf.Lerp(c.fieldOfView,fov,1);
		else
			c.fieldOfView = Mathf.Lerp(c.fieldOfView,60,1);
	}
}