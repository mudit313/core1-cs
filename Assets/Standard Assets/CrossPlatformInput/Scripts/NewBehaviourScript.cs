﻿using UnityEngine;
using System.Collections;
public class NewBehaviourScript : MonoBehaviour {
	public static int currentWeapon;
	public static Transform[] weapons = new Transform[10];

	public void Weapon(int num) {
		currentWeapon = num;
		for(int i = 0; i < weapons.Length; i++) {
			if(i == num)
				weapons[i].gameObject.SetActive(true);
			else
				weapons[i].gameObject.SetActive(false);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Alpha1))
			Weapon(0);
		if(Input.GetKeyDown(KeyCode.Alpha2))
			Weapon(1);
		if(Input.GetKeyDown(KeyCode.Alpha3))
			Weapon(2);
	}
}
